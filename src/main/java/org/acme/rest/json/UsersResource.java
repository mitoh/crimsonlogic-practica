package org.acme.rest.json;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UsersResource {

    @Inject
    UsersService usersService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public List<String> getUsers() {
        return usersService.getUsers();
    }
}